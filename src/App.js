import React, {Component,Fragment} from 'react';
import {Route, Switch,withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/usersActions";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Products from "./containers/Products/Products";
import AddProduct from "./containers/AddNewProduct/AddProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import SingleProduct from "./containers/SingleProduct/SingleProduct";

class App extends Component {
    render() {
        return (
            <Fragment>
              <header>
                <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
              </header>
                <Switch>
                  <Route path="/" exact component={Products}/>
                  <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/products/:id" exact component={SingleProduct}/>
                    <Route path="/add_position" exact component={AddProduct}/>
                </Switch>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
  user: state.users.user
});
const mapDispatchToProps = dispatch=>({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));