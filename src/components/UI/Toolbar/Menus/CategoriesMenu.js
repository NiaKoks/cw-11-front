import React from 'react';
import {Nav, NavItem, NavLink} from "reactstrap";

const CategoriesMenu = () => (
        <Nav vertical style={{width:'15%'}}>
            <NavItem>
                <NavLink href="/">All</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/products/cat_1">Category 1</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/products/cat_2">Category 2</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/products/cat_3">Category 3</NavLink>
            </NavItem>
        </Nav>
    );

export default CategoriesMenu;