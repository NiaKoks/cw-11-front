import React, {Fragment}from 'react';
import PropTypes from 'prop-types'
import {Button, Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Container} from "reactstrap";
import {Link} from "react-router-dom";

const style = {
    images:{
        width: '100px',
        height: '100px',
    },
    cards:{
        marginLeft: '10%'
    },
    container:{
        marginTop: '-5%',
        marginBottom: '7%'
    },
    button:{
        marginLeft: '5px',
        width: '10%',
        marginBottom: '5px'
    }
};

const ProductListItem = props => {
    return (
        <Fragment>
            <Container style={style.container}>
                <Card style={style.cards}>
                    <CardBody>
                        <CardImg src={props.img} style={style.images}/>
                        <Link to={'/products/'+ props._id}> {props.title} </Link>
                        <CardTitle>{props.category}</CardTitle>
                        <CardSubtitle>{props.seller}</CardSubtitle>
                        <CardText>{props.description}</CardText>
                        <strong style={{marginLeft: '10px'}}>
                            {props.price} KGS
                        </strong>
                    </CardBody>
                    <Button color="danger" style={style.button} disabled>X</Button>
                </Card>
            </Container>
        </Fragment>
    )
};

ProductListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    price:PropTypes.string.isRequired,
    title: PropTypes.string,
    seller: PropTypes.string,
    description: PropTypes.string
};

export default ProductListItem;