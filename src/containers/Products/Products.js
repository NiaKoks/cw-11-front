import React, {Component,Fragment} from 'react';
import {fetchProducts} from "../../store/actions/productsActions";
import {connect} from "react-redux";

import ProductListItem from "../../components/ProductListItem/ProductListItem";
import CategoriesMenu from "../../components/UI/Toolbar/Menus/CategoriesMenu";

class Products extends Component {
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.onFetchProducts(id);
    }

    render() {
        return (
            <Fragment>
                <CategoriesMenu category={this.props.category}/>
                {this.props.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        _id={product._id}
                        title={product.title}
                        seller={product.seller}
                        price={product.price}
                        description={product.description}
                        img={product.img}
                    />
                    )
                )}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.products.products
});

const mapDispatchToProps = dispatch => ({
    onFetchProducts: (id) => dispatch(fetchProducts(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);