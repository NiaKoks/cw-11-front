import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";

const style = {
    width: '100px',
    height: '100px',
};


const SingleProduct = (props) => {
    return (
        props.products ? props.products.map((product,index)=>{
            return(
                <Card>
                    <CardBody style={{marginBottom: '10px'}}>
                        <CardImg img={props.img} style={style}/>
                        <Link to={'/products/'+ props._id}> {props.title} </Link>
                        <CardTitle>{props.category}</CardTitle>
                        <CardSubtitle>{props.seller}</CardSubtitle>
                        <CardText>{props.description}</CardText>
                        <strong style={{marginLeft: '10px'}}>
                            {props.price} KGS
                        </strong>
                    </CardBody>
                </Card>
            )
        }) : null
    );
};
SingleProduct.propTypes ={
    img: PropTypes.string
};
export default SingleProduct;