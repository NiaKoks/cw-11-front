import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";

class Register extends Component {

  state ={
    username: '',
    password: '',
    displayname: '',
    phonenumber: ''
  };

  inputChangeHandler = event =>{
    this.setState({
      [event.target.name]:event.target.value
    })
  };

  submitFormHandler = event =>{
    event.preventDefault();
    this.props.registerUser({...this.state});
  };

  catchFieldErr = fieldName =>{
    return this.props.error && this.props.error.errors &&
        this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
  }

  render() {
    return (
        <Fragment>
          <h2>Create Your Profile:</h2>

          <Form onSubmit={this.submitFormHandler}>
            <FormElement
                propertyName="username"
                title="username"
                type="text"
                value={this.state.username}
                onChange={this.inputChangeHandler}
                error={this.catchFieldErr('username')}
                placeholder="Enter your desire username"
                autoComplete="new-username"
            />
            <FormElement
                propertyName="password"
                title="password"
                type="password"
                value={this.state.password}
                onChange={this.inputChangeHandler}
                error={this.catchFieldErr('password')}
                placeholder="Enter your desire password"
                autoComplete="new-password"
            />
            <FormElement
                propertyName="displayname"
                title="displayname"
                type="text"
                value={this.state.displayname}
                onChange={this.inputChangeHandler}
                error={this.catchFieldErr('displayname')}
                placeholder="Enter your seller name"
            />
            <FormElement
                propertyName="phonenumber"
                title="phonenumber"
                type="number"
                value={this.state.phonenumber}
                onChange={this.inputChangeHandler}
                error={this.catchFieldErr('phonenumber')}
                placeholder="Enter your phonenumber"
            />
            <FormGroup row>
              <Col sm={{offset: 2, size: 10}}>
                <Button type="submit" color="primary">
                  Register
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);