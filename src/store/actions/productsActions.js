import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});

export const fetchProducts = () => {
    return dispatch => {
        return axios.get('/products').then(
            response => dispatch(fetchProductsSuccess(response.data))
        );
    };
};

export const createProduct = productData => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if (user === null){
            dispatch(push('/login'));
        } else {
            return axios.post('/products', productData,{headers:{'Authorization': user.token}}).then(
                () => dispatch(createProductSuccess())
            );
        }
    };
};

export const getSingleProduct = (id) => {
    return dispatch => {
        return axios.get('/posts/' + id).then(
            response => {
                console.log(response.data);
                return dispatch(fetchProductsSuccess(response.data));
            },

        );
    }
};